## Summary assignment on Git  
### What is Git?  
> Git is version-control software, you easily keep track of every revision you and your team makes during development of your software.
> Git is a **Distributed Version Control System**, so git doesn't rely on a central server for storing the data.

#### Git Workflow  
 * What is a Repository?  
 A repository a.k.a. **repo** is a collection of source code.  
 
**There are four fundamental elements in the Git Workflow.**  
1. Working Directory
2. Staging Area
3. Local Repository
4. Remote Repository


![Simple Git Workflow](PicturesInUse/GitWorkflow.png)  

If you consider a file in your Working Directory,it can be in three possible states.  
1. **It can be staged**: Which means the files with the updated changes are marked to be committed to the local repository but not yet committed.  
2. **It can be modified**: Which means the files with the updates changes are not yet stored in the local repository.  
3. **It can be committed**: Which means that the changes you made to your file are safely stored in the local repository.  

* *$ git add* is a command used to add a file that is in the working directory to the staging area.
* *$ git commit* is a command used to add all files that are staged to the local repository.
* *$ git push* is a command used to add all committed files in the local repository to the remote repository.
* *$ git fetch* is a command used to get files from the remote repository to the local repository but not into the working directory.
* *$ git merge* is a command used to get files from the remote repo into the working directory.
* *$ git pull* is a command used to get files from the remote repo directly into the working directory. It is equivalent to a *git fetch* and a *git merge*.

### What is GitLab?
> It is Repository Management Services and they play a crucial role in the mordern software development. Git is the tool, GitLab is the services for projects that use Git.

**Few simple rules to always follow when using Git:**
* **Rule 1:** Create a Git repository for every new project.
* **Rule 2:** Create a new branch for every new feature.

### Commands
* **Initialize a new Git repository: Git init**  
Everything you code is tracked in the repository. To initialize a git repo, use this command while inside the project folder. This will create a .git folder.  
> git init

* **Git add**  
This command adds one or all changes files to the staging area.  

1. To add just a specific file to staging:  
> git add filename.py  
2. To stage new, modofied or deleted files:
> git add -A
3. To stage new and modified files:
> git add .
4. To stage modified and deleted files:
> git add -u

* **Git commit**
This command records the file in the version history. The -m means that a commit message follows. This meassage ia a custom one and you should use it to refer in future.
> git commit -m "your text"

* **Git status**
This command will list files in green or red colors. Green files have been added to the stage but not committed yet. Files marked as red are the ones not yet added to the stage.
>git status

* **Working with Branches**
1. This will create a new branch:
> git branch branch_name
2. To switch from one branch to another:
> git checkout branch_name
3. To create new branch and switch to it automatically:
> git checkout -b branch_name
4. To list all branches and see on what branch you currently are:
> git branch
5. This command will list the version history for the current branch:
> git log

* **Push and Pull**
1. This command sends the committed changes to remote repo:
> git push
2. To pull the changes from the remote server to your local computer:
> git pull

* **Tips and Tricks**
1. Throw away all you uncommitted changes:
> git reset --hard
2. Edit a commit message:
> git commit --amend -m "New Message"
3. Remove file from git without removing it from computer
> git reset file_name
4. make git ignore a file from adding to git:
> echo file_name >> .gitignore


