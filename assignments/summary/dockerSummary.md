## Summary assignment on Docker
### What is Container?
* Container provides hardware virtualization, a container provides operating-system-level virtualization by abstracting the "user-space".
* Container has a private space for processing, can execute commands as root, have a private network interface and IP address, allow custom routes and iptable rules, can mount file systems, and etc.
* Containers *share* the host system's kernel with other containers.
* Containers package up just the user space, and not the kernel or vitual hardware.
* Each container gets its own isolated user space to allow multiple containers to run on a single host machine. The only parts that are created from scratch across containers. The only parts that are created from scratch are the bins and libs, which makes containers lightweight.

![Container Structure](PicturesInUse/ContainerStruct.png)

### What is Docker?
* Docker is an open-source project based on Linux containers. It uses Linux Kernel features like namespaces and control groups to create containers on top of an operating system.

**Advantages of Docker**
1. **Ease of use**: Docker has made it much easier for anyone--developers, systems admins, architects and other-- to take advantage of containers in order to quickly build and test portable applications.

2. **Speed**: Docker containers are very lightweight and fast. Since containers are sandboxed environments running on the kernel, they take up fewer resources.

3. **Docker Hub**: Docker users benefit from the increasingly rich ecosystem of Docker Hub, which you can think of as an "app store for Docker images."

4. **Modularity and Scalability**: Docker makes it easy to break out your application's functionality into individual containers.With Docker, it's become easier to link these containers together to create you application, making it easy to scale or update components independently in the future.

**Fundamental Docker Concepts**

![Docker](PicturesInUse/Docker.png)

1. **Docker Engine**  
Docker engine is the layer on which Docker runs. It's a ligthtweight runtime and tooling that manages containers, images, builds, and more. It is made up of:  
* A Docker Daemon that runs in the host computer.
* A Docker Client that then communicates with the Docker Daemon to execute commands.
* A REST API for interfacing with the Docker Daemon remotely.

2. **Docker Client**  
The Docker Client is what you, as the end-user of Docker, communicate with. Think of it as the UI for Docker.

3. **Docker Daemon**  
The Docker daemon is what actually executes commands sent to the Docker Client-- like building , running and distributing your containers.  
The Docker Daemon runs on the host machine, but as a user, you never communicate directly with Daemon.

4. **Docker File**  
A Dockerfile is where you write the instructions to build a Docker image.  
Once you have got Dockerfile set up, you can use the *docker build* command to build an image from it.

5. **Docker Image**
Images are read only templates that you build from a set of instructions written in you Dockerfile.  
Images define both what you want your packaged application and it's dependencies to look like and what processes to run when it's launched.  
The Docker image is built using a Dockerfile. Each instruction in the Dockerfile adds a new “layer” to the image, with layers representing a portion of the images file system that either adds to or replaces the layer below it.  
Layers are key to Docker’s lightweight yet powerful structure. 

![Docker Container Structure](PicturesInUse/dockerStruct.png)

